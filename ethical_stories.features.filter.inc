<?php
/**
 * @file
 * ethical_stories.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function ethical_stories_filter_default_formats() {
  $formats = array();

  // Exported format: Plain text with formatting.
  $formats['plain_text_with_formatting'] = array(
    'format' => 'plain_text_with_formatting',
    'name' => 'Plain text with formatting',
    'cache' => 1,
    'status' => 1,
    'weight' => 0,
    'filters' => array(
      'filter_html_escape' => array(
        'weight' => -10,
        'status' => 1,
        'settings' => array(),
      ),
      'filter_autop' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(),
      ),
      'spamspan' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(
          'spamspan_at' => ' [at] ',
          'spamspan_use_graphic' => 0,
          'spamspan_dot_enable' => 0,
          'spamspan_dot' => ' [dot] ',
          'spamspan_use_form' => FALSE,
          'spamspan_use_url' => '<a href="/#formname?goto=#email">#displaytext</a>',
          'spamspan_email_encode' => TRUE,
          'spamspan_email_default' => 'contact_us_general_enquiry',
        ),
      ),
    ),
  );

  return $formats;
}
