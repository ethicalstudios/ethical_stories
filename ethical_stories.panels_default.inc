<?php
/**
 * @file
 * ethical_stories.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function ethical_stories_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'oe_story_header';
  $mini->category = '';
  $mini->admin_title = 'Story header';
  $mini->admin_description = '';
  $mini->requiredcontexts = array(
    0 => array(
      'identifier' => 'Node',
      'keyword' => 'node',
      'name' => 'entity:node',
      'entity_id' => '',
      'id' => 1,
    ),
  );
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'oe_story_header';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '890b36b7-fb61-47f5-bde9-27399ef8f10b';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-f25328e0-fd3c-465d-a64e-27b4ea64ac42';
  $pane->panel = 'contentmain';
  $pane->type = 'node_title';
  $pane->subtype = 'node_title';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'link' => 0,
    'markup' => 'h1',
    'id' => '',
    'class' => '',
    'context' => 'requiredcontext_entity:node_1',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'f25328e0-fd3c-465d-a64e-27b4ea64ac42';
  $display->content['new-f25328e0-fd3c-465d-a64e-27b4ea64ac42'] = $pane;
  $display->panels['contentmain'][0] = 'new-f25328e0-fd3c-465d-a64e-27b4ea64ac42';
  $pane = new stdClass();
  $pane->pid = 'new-783ca526-ca22-4a5d-a8db-897ab064bbb9';
  $pane->panel = 'contentmain';
  $pane->type = 'node_body';
  $pane->subtype = 'node_body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => 'requiredcontext_entity:node_1',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '783ca526-ca22-4a5d-a8db-897ab064bbb9';
  $display->content['new-783ca526-ca22-4a5d-a8db-897ab064bbb9'] = $pane;
  $display->panels['contentmain'][1] = 'new-783ca526-ca22-4a5d-a8db-897ab064bbb9';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $mini->display = $display;
  $export['oe_story_header'] = $mini;

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'oe_story_navigation';
  $mini->category = '';
  $mini->admin_title = 'Story navigation';
  $mini->admin_description = '';
  $mini->requiredcontexts = array(
    0 => array(
      'identifier' => 'Node',
      'keyword' => 'node',
      'name' => 'entity:node',
      'entity_id' => '',
      'id' => 1,
    ),
  );
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'oe_empty';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'c4245e1e-7db0-42dd-be76-e9d9f64ddf02';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-d8a4ec81-d8c5-483c-b6d8-73b5827ba55a';
  $pane->panel = 'contentmain';
  $pane->type = 'ethical_stories_nav_pane';
  $pane->subtype = 'ethical_stories_nav_pane';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd8a4ec81-d8c5-483c-b6d8-73b5827ba55a';
  $display->content['new-d8a4ec81-d8c5-483c-b6d8-73b5827ba55a'] = $pane;
  $display->panels['contentmain'][0] = 'new-d8a4ec81-d8c5-483c-b6d8-73b5827ba55a';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $mini->display = $display;
  $export['oe_story_navigation'] = $mini;

  return $export;
}
