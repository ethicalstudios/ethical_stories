(function ($) {

  Drupal.behaviors.EthicalStories = {
    attach: function (context, settings) {

      if($('div.panels-ipe-editing').length){
        //hide the panel delete, edit and style buttons for panels that editors cannot re-add if they accidentally delete
        $('div.panels-ipe-portlet-content>div.pane-ethical-stories-nav-pane').each(function(){
          $(this).closest('.panels-ipe-portlet-wrapper').find('ul.panels-ipe-linkbar').hide();
        });
      }
      if(!$('#navbar-bar').length){
        $(function () {
          $('#story-nav').stickyNavbar();
        });
      }
    }
  };

})(jQuery);

