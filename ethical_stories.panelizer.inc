<?php
/**
 * @file
 * ethical_stories.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function ethical_stories_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:oe_story:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'oe_story';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = 'oe-story-full';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = 'oe-story-full';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'striped';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'header' => NULL,
      'stripe1' => array(
        'story_stripe_title' => 'A title',
        'story_stripe_bg_style' => 'a',
      ),
      'stripe2' => array(
        'story_stripe_title' => 'A title',
        'story_stripe_bg_style' => 'b',
      ),
      'stripe3' => array(
        'story_stripe_title' => 'A title',
        'story_stripe_bg_style' => 'a',
      ),
      'stripe4' => array(
        'story_stripe_title' => 'A title',
        'story_stripe_bg_style' => 'b',
      ),
      'stripe5' => array(
        'story_stripe_title' => 'A title',
        'story_stripe_bg_style' => 'a',
      ),
      'stripe6' => array(
        'story_stripe_title' => 'A title',
        'story_stripe_bg_style' => 'b',
      ),
      'stripe7' => array(
        'story_stripe_title' => 'A title',
        'story_stripe_bg_style' => 'a',
      ),
      'stripe8' => array(
        'story_stripe_title' => 'A title',
        'story_stripe_bg_style' => 'b',
      ),
      'footer' => NULL,
    ),
    'stripe1' => array(
      'style' => 'story_stripe',
    ),
    'stripe2' => array(
      'style' => 'story_stripe',
    ),
    'stripe3' => array(
      'style' => 'story_stripe',
    ),
    'stripe4' => array(
      'style' => 'story_stripe',
    ),
    'stripe5' => array(
      'style' => 'story_stripe',
    ),
    'stripe6' => array(
      'style' => 'story_stripe',
    ),
    'stripe7' => array(
      'style' => 'story_stripe',
    ),
    'stripe8' => array(
      'style' => 'story_stripe',
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '40b38f6b-a4b7-403f-a2b8-9da5d3feb0d3';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-3ba597d1-89ef-46c8-9f1e-e130f1d085d1';
  $pane->panel = 'footer';
  $pane->type = 'block';
  $pane->subtype = 'easy_social-easy_social_block_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '3ba597d1-89ef-46c8-9f1e-e130f1d085d1';
  $display->content['new-3ba597d1-89ef-46c8-9f1e-e130f1d085d1'] = $pane;
  $display->panels['footer'][0] = 'new-3ba597d1-89ef-46c8-9f1e-e130f1d085d1';
  $pane = new stdClass();
  $pane->pid = 'new-3de5b555-1bd3-4497-b28c-8c13f102cfc0';
  $pane->panel = 'header';
  $pane->type = 'panels_mini';
  $pane->subtype = 'oe_story_header';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(
      0 => 'panelizer',
    ),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array(
    'type' => 'immovable',
    'regions' => array(
      'header' => 'header',
    ),
  );
  $pane->uuid = '3de5b555-1bd3-4497-b28c-8c13f102cfc0';
  $display->content['new-3de5b555-1bd3-4497-b28c-8c13f102cfc0'] = $pane;
  $display->panels['header'][0] = 'new-3de5b555-1bd3-4497-b28c-8c13f102cfc0';
  $pane = new stdClass();
  $pane->pid = 'new-14b2bd3b-ef68-4120-b78f-da379e5e2095';
  $pane->panel = 'header';
  $pane->type = 'panels_mini';
  $pane->subtype = 'oe_story_navigation';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(
      0 => 'panelizer',
    ),
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array(
    'type' => 'immovable',
    'regions' => array(
      'header' => 'header',
    ),
  );
  $pane->uuid = '14b2bd3b-ef68-4120-b78f-da379e5e2095';
  $display->content['new-14b2bd3b-ef68-4120-b78f-da379e5e2095'] = $pane;
  $display->panels['header'][1] = 'new-14b2bd3b-ef68-4120-b78f-da379e5e2095';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:oe_story:default'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:oe_story:default:featured';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'oe_story';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'featured';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array();
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'b53415a5-b327-43f0-a4a2-302a5788370f';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-da40f861-049e-46e1-832d-081bd4a90bcd';
  $pane->panel = 'center';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'above',
    'formatter' => 'hidden',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'da40f861-049e-46e1-832d-081bd4a90bcd';
  $display->content['new-da40f861-049e-46e1-832d-081bd4a90bcd'] = $pane;
  $display->panels['center'][0] = 'new-da40f861-049e-46e1-832d-081bd4a90bcd';
  $pane = new stdClass();
  $pane->pid = 'new-a2da4999-2ee2-4a36-8f65-f9871cad9322';
  $pane->panel = 'center';
  $pane->type = 'node_links';
  $pane->subtype = 'node_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => FALSE,
    'override_title_text' => '',
    'build_mode' => 'featured',
    'identifier' => '',
    'link' => TRUE,
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array(
    'css_class' => 'link-wrapper',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'a2da4999-2ee2-4a36-8f65-f9871cad9322';
  $display->content['new-a2da4999-2ee2-4a36-8f65-f9871cad9322'] = $pane;
  $display->panels['center'][1] = 'new-a2da4999-2ee2-4a36-8f65-f9871cad9322';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-a2da4999-2ee2-4a36-8f65-f9871cad9322';
  $panelizer->display = $display;
  $export['node:oe_story:default:featured'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:oe_story:default:teaser';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'oe_story';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'teaser';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array();
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'f4494f7b-902d-4d83-8ca5-12bc6a4a8127';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-324f2f36-28e6-43bb-9fe2-bc0ebff111f8';
  $pane->panel = 'center';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'text_summary_or_trimmed',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'trim_length' => 600,
    ),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '324f2f36-28e6-43bb-9fe2-bc0ebff111f8';
  $display->content['new-324f2f36-28e6-43bb-9fe2-bc0ebff111f8'] = $pane;
  $display->panels['center'][0] = 'new-324f2f36-28e6-43bb-9fe2-bc0ebff111f8';
  $pane = new stdClass();
  $pane->pid = 'new-cc0ba7c9-896d-4171-a096-2d3a28797670';
  $pane->panel = 'center';
  $pane->type = 'node_links';
  $pane->subtype = 'node_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => FALSE,
    'override_title_text' => '',
    'build_mode' => 'teaser',
    'identifier' => '',
    'link' => TRUE,
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array(
    'css_class' => 'link-wrapper',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'cc0ba7c9-896d-4171-a096-2d3a28797670';
  $display->content['new-cc0ba7c9-896d-4171-a096-2d3a28797670'] = $pane;
  $display->panels['center'][1] = 'new-cc0ba7c9-896d-4171-a096-2d3a28797670';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-cc0ba7c9-896d-4171-a096-2d3a28797670';
  $panelizer->display = $display;
  $export['node:oe_story:default:teaser'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:oe_story:default:tiny';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'oe_story';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'tiny';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array();
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '2d51b7c5-ef5b-4a9e-9229-761e02960742';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-d37ae10d-6bb4-4cfd-84c8-beb1b73333cf';
  $pane->panel = 'center';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'above',
    'formatter' => 'hidden',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd37ae10d-6bb4-4cfd-84c8-beb1b73333cf';
  $display->content['new-d37ae10d-6bb4-4cfd-84c8-beb1b73333cf'] = $pane;
  $display->panels['center'][0] = 'new-d37ae10d-6bb4-4cfd-84c8-beb1b73333cf';
  $pane = new stdClass();
  $pane->pid = 'new-47106808-7853-4e0f-bfb1-99339da8b4b8';
  $pane->panel = 'center';
  $pane->type = 'node_links';
  $pane->subtype = 'node_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => FALSE,
    'override_title_text' => '',
    'build_mode' => 'tiny',
    'identifier' => '',
    'link' => TRUE,
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array(
    'css_class' => 'link-wrapper',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '47106808-7853-4e0f-bfb1-99339da8b4b8';
  $display->content['new-47106808-7853-4e0f-bfb1-99339da8b4b8'] = $pane;
  $display->panels['center'][1] = 'new-47106808-7853-4e0f-bfb1-99339da8b4b8';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-47106808-7853-4e0f-bfb1-99339da8b4b8';
  $panelizer->display = $display;
  $export['node:oe_story:default:tiny'] = $panelizer;

  return $export;
}
