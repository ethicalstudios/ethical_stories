<?php
/**
 * @file
 * Template for OE Simple.
 *
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 * panel of the layout. This layout supports the following sections:
 */

  $nid = '';
  $style = '';
  $context = current($variables['display']->context);

  if(!empty($context->argument)){
    $nid = $context->argument;
  }
  if(is_numeric($nid)){
    $node = node_load($nid);

    if(!empty($node->field_featured_image)){
      $image = field_get_items('node', $node, 'field_featured_image');
      $image_path = image_style_url('oe_story_banner', $image[0]['uri']);

      if(!empty($image_path)){
        $style = 'background-image:url(' . $image_path . ')';
      }
    }
  }

?>
<div class="panel-display clearfix <?php if (!empty($class)) { print $class; } ?>" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?> <?php if (!empty($style)) { print "style=\"$style\""; } ?>>
  <div class="container">
    <?php print $content['contentmain']; ?>
  </div>
</div>
