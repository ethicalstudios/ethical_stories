<?php
// Plugin definition
$plugin = array(
  'title' => t('Striped'),
  'icon' => 'striped.png',
  'category' => t('Open Ethical'),
  'theme' => 'striped',
  'regions' => array(
    'header' => t('Header'),
    'stripe1' => t('First stripe'),
    'stripe2' => t('Second stripe'),
    'stripe3' => t('Third stripe'),
    'stripe4' => t('Fourth stripe'),
    'stripe5' => t('Fifth stripe'),
    'stripe6' => t('Sixth stripe'),
    'stripe7' => t('Seventh stripe'),
    'stripe8' => t('Eighth stripe'),
    'footer' => t('Footer'),
  ),
);
