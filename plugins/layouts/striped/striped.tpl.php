<?php
/**
 * @file
 * Template for OE Striped layout.
 *
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 * panel of the layout. This layout supports the following sections:
 */

$field_oe_story_section_titles = array();
$context = current($variables['display']->context);
$regions = $variables['display']->renderer_handler->prepared['regions'];
$nid = '';

if(!empty($context->argument)){
  $nid = $context->argument;
}

$style = '';
if (!empty($nid)) {
  $node = $context->data;

  if(!empty($node->field_oe_story_section_titles)){
    $field_oe_story_section_titles = field_get_items('node', $node, 'field_oe_story_section_titles');
  }
}
?>

<div class="panel-display striped clearfix <?php if (!empty($classes)) { print $classes; } ?><?php if (!empty($class)) { print $class; } ?>" id="oe-story-full">

  <div class="row striped-header">
    <div class="col-md-12 panel-panel">
      <div class="panel-panel-inner">
        <?php print $content['header']; ?>
      </div>
    </div>
  </div>
  <?php if(count($field_oe_story_section_titles) > 0){ ?>
  <section>
    <div id="<?php print transliteration_clean_filename('stripe1-' . $field_oe_story_section_titles[0]['safe_value']); ?>" class="scrollto row striped-stripe stripe-<?php print $regions['stripe1']['style settings']['story_stripe_bg_style'];?>">
      <h1 class="stripe-heading"><span class="stripe-heading-icn"></span><?php print $field_oe_story_section_titles[0]['safe_value']; ?></h1>
      <?php print $content['stripe1']; ?>
    </div>
  </section>
  <?php } ?>
  <?php if(count($field_oe_story_section_titles) > 1){ ?>
  <section>
    <div id="<?php print transliteration_clean_filename('stripe2-' . $field_oe_story_section_titles[1]['safe_value']); ?>" class="scrollto row striped-stripe stripe-<?php print $regions['stripe2']['style settings']['story_stripe_bg_style'];?>">
      <h1 class="stripe-heading"><span class="stripe-heading-icn"></span><?php print $field_oe_story_section_titles[1]['safe_value']; ?></h1>
      <?php print $content['stripe2']; ?>
    </div>
  </section>
  <?php } ?>
  <?php if(count($field_oe_story_section_titles) > 2){ ?>
  <section>
    <div id="<?php print transliteration_clean_filename('stripe3-' . $field_oe_story_section_titles[2]['safe_value']); ?>" class="scrollto row striped-stripe stripe-<?php print $regions['stripe3']['style settings']['story_stripe_bg_style'];?>">
      <h1 class="stripe-heading"><span class="stripe-heading-icn"></span><?php print $field_oe_story_section_titles[2]['safe_value']; ?></h1>
      <?php print $content['stripe3']; ?>
    </div>
  </section>
  <?php } ?>
  <?php if(count($field_oe_story_section_titles) > 3){ ?>
  <section>
    <div id="<?php print transliteration_clean_filename('stripe4-' . $field_oe_story_section_titles[3]['safe_value']); ?>" class="scrollto row striped-stripe stripe-<?php print $regions['stripe4']['style settings']['story_stripe_bg_style'];?>">
      <h1 class="stripe-heading"><span class="stripe-heading-icn"></span><?php print $field_oe_story_section_titles[3]['safe_value']; ?></h1>
      <?php print $content['stripe4']; ?>
    </div>
  </section>
  <?php } ?>
  <?php if(count($field_oe_story_section_titles) > 4){ ?>
  <section>
    <div id="<?php print transliteration_clean_filename('stripe5-' . $field_oe_story_section_titles[4]['safe_value']); ?>" class="scrollto row striped-stripe stripe-<?php print $regions['stripe5']['style settings']['story_stripe_bg_style'];?>">
      <h1 class="stripe-heading"><span class="stripe-heading-icn"></span><?php print $field_oe_story_section_titles[4]['safe_value']; ?></h1>
      <?php print $content['stripe5']; ?>
    </div>
  </section>
  <?php } ?>
  <?php if(count($field_oe_story_section_titles) > 5){ ?>
  <section>
    <div id="<?php print transliteration_clean_filename('stripe6-' . $field_oe_story_section_titles[5]['safe_value']); ?>" class="scrollto row striped-stripe stripe-<?php print $regions['stripe6']['style settings']['story_stripe_bg_style'];?>">
      <h1 class="stripe-heading"><span class="stripe-heading-icn"></span><?php print $field_oe_story_section_titles[5]['safe_value']; ?></h1>
      <?php print $content['stripe6']; ?>
    </div>
  </section>
  <?php } ?>
  <?php if(count($field_oe_story_section_titles) > 6){ ?>
  <section>
    <div id="<?php print transliteration_clean_filename('stripe7-' . $field_oe_story_section_titles[6]['safe_value']); ?>" class="scrollto row striped-stripe stripe-<?php print $regions['stripe7']['style settings']['story_stripe_bg_style'];?>">
      <h1 class="stripe-heading"><span class="stripe-heading-icn"></span><?php print $field_oe_story_section_titles[6]['safe_value']; ?></h1>
      <?php print $content['stripe7']; ?>
    </div>
  </section>
  <?php } ?>
  <?php if(count($field_oe_story_section_titles) > 7){ ?>
  <section>
    <div id="<?php print transliteration_clean_filename('stripe8-' . $field_oe_story_section_titles[7]['safe_value']); ?>" class="scrollto row striped-stripe stripe-<?php print $regions['stripe8']['style settings']['story_stripe_bg_style'];?>">
      <h1 class="stripe-heading"><span class="stripe-heading-icn"></span><?php print $field_oe_story_section_titles[7]['safe_value']; ?></h1>
      <?php print $content['stripe8']; ?>
    </div>
  </section>
  <?php } ?>
  <div class="container">
    <div class="row striped-footer">
      <div class="col-md-12 panel-panel">
        <div class="panel-panel-inner">
          <?php print $content['footer']; ?>
        </div>
      </div>
    </div>
  </div>
</div><!-- /.striped -->
