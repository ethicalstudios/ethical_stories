<?php

// Plugin definition
$plugin = array(
  'title' => t('Story stripe'),
  'description' => t('Display different stripe background colours/textures.'),
  'render region' => 'ethical_stories_story_stripe_style_render_region',
  'settings form' => 'ethical_stories_story_stripe_style_settings_form',
  'hook theme' => array(
    'ethical_stories_story_strip' => array(
      'variables' => array('content' => NULL),
      'template' => 'ethical-stories-story-strip',
      'path' => drupal_get_path('module', 'ethical_stories') . '/plugins/styles/story_stripe',
    ),
  ),
  'weight' => -15,
);

/**
 * Render callback.
 *
 * @ingroup themeable
 */
function theme_ethical_stories_story_stripe_style_render_region($vars) {

  $output = '';
  $num = 0;
  $stripe_id = $vars['region_id'];

  $prefix = 'stripe';

  if (substr($vars['region_id'], 0, strlen($prefix)) == $prefix) {
      $num = substr($vars['region_id'], strlen($prefix));
  }

  $context = current($vars['display']->context);
  $nid = $context->argument;
  $node = node_load($nid);

  $field_oe_story_section_titles = field_view_field('node', $node, 'field_oe_story_section_titles');


  if(!empty($field_oe_story_section_titles['#items'][$num - 1]['safe_value'])){

    $style_attributes['class'] = array();

    // Add our classes to the attrubutes array, if any defined
    if (!empty($vars['settings']['class'])) {
      $style_attributes['class'] = explode(' ', $vars['settings']['class']);
    }

    $content = implode($vars['panes']);

    $output = theme('ethical_stories_story_strip', array(
        'content' => $content,
        'style_attributes' => $style_attributes,
      ));
  }

  return $output;
}

function ethical_stories_story_stripe_style_settings_form($style_settings) {
  $form['story_stripe_bg_style'] = array(
    '#type' => 'select',
    '#title' => t('Background style.'),
    '#options' => array('a' => t('Style A'), 'b' => t('Style B'), 'c' => t('Style C'), ),
    '#default_value' => (isset($style_settings['story_stripe_bg_style'])) ? $style_settings['story_stripe_bg_style'] : 'a',
  );

  return $form;
}
