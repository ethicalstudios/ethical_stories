<div<?php print drupal_attributes($style_attributes); ?>>
  <div class="container">
    <div class="col-md-12">
      <?php print render($content); ?>
    </div>
  </div>
</div>
